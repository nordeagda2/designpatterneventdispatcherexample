package events;

import kfc.Order;
import listeners.INewOrderListener;

import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class NewOrderArrivedEvent implements IEvent {
    private Order o ;

    public NewOrderArrivedEvent(Order o) {
        this.o = o;
    }

    @Override
    public void run() {
        List listeners = EventDispatcher.instance.getAllObjectsImplementingInterface(INewOrderListener.class);

        List<INewOrderListener> listListeners = (List<INewOrderListener>) listeners;
        for (INewOrderListener listener : listListeners) {
            listener.newOrderArrived(o);
        }

    }
}
