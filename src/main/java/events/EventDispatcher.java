package events;

import org.apache.commons.lang3.ClassUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by amen on 9/14/17.
 */
public class EventDispatcher {
    public static EventDispatcher instance = new EventDispatcher();

    private Map<Class<?>, List<Object>> map = new HashMap<>();

    private EventDispatcher() {
    }

    public void registerObject(Object o) {
        List<Class<?>> interfacesImplementedByObject = ClassUtils.getAllInterfaces(o.getClass());
        for (Class<?> classtype : interfacesImplementedByObject) {
            List<Object> objects = map.get(classtype);
            if (objects == null) { // jeśli lista nie istnieje (nie ma obiektu
                // który implementuje ten interfejs)
                objects = new ArrayList<>(); // tworze nowa liste
            }
            objects.add(o); // dodaje obiekt do listy
            map.put(classtype, objects); // umieszczam listę z powrotem w mapie

        }
    }

    public List<Object> getAllObjectsImplementingInterface(Class<?> clas) {
        return map.get(clas);
    }

    public void unregisterObject(Object o) {

    }

    public void dispatch(IEvent e) {
        e.run();

    }
}
