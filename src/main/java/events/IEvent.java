package events;

/**
 * Created by amen on 9/14/17.
 */
public interface IEvent {
    void run();
}
