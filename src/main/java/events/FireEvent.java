package events;

import listeners.IFireListener;
import listeners.INewOrderListener;

import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class FireEvent implements IEvent {

    @Override
    public void run() {
        List listeners = EventDispatcher.instance.getAllObjectsImplementingInterface(IFireListener.class);

        List<IFireListener> listListeners = (List<IFireListener>) listeners;
        for (IFireListener listener : listListeners) {
            listener.fire();
        }

    }
}
