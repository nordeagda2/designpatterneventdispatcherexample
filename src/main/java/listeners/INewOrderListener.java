package listeners;

import kfc.Order;

/**
 * Created by amen on 9/14/17.
 */
public interface INewOrderListener {
    void newOrderArrived(Order o);
}
