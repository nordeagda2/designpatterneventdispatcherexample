package listeners;

/**
 * Created by amen on 9/14/17.
 */
public interface IBurgerProduced {
    void burgerProduced();
}
