package kfc;

import events.EventDispatcher;
import events.NewOrderArrivedEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class Kitchen {
    private List<Order> orders = new ArrayList<>();

    public Kitchen() {
//        List<Worker> workers = Arrays.asList(new ChickenWorker(), new CashRegisterWorker(),
//                new BurgerWorker(), new BurgerWorker(),
//                new BurgerWorker(), new BurgerWorker(), new BurgerWorker());

        new BurgerWorker();
        new ChickenWorker();
//        for (Worker w : workers) {
//            EventDispatcher.instance.registerObject(w);
//        }
    }

    public void addOrder(Order o) {
        EventDispatcher.instance.dispatch(new NewOrderArrivedEvent(o));
        orders.add(o);
    }

}
