package kfc;

import events.EventDispatcher;
import events.FireEvent;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by amen on 9/14/17.
 */
public class Main {
    public static void main(String[] args) {
        Kitchen k = new Kitchen();

        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            if(line.startsWith("fire")){
                EventDispatcher.instance.dispatch(new FireEvent());
            }
            k.addOrder(new Order(Arrays.asList("burger")));
        }
    }
}
