package kfc;

import listeners.IFireListener;
import listeners.INewOrderListener;

/**
 * Created by amen on 9/14/17.
 */
public class BurgerWorker extends Worker implements INewOrderListener{

    @Override
    public void newOrderArrived(Order o) {
        System.out.println(o);
    }
}
