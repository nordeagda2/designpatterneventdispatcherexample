package kfc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amen on 9/14/17.
 */
public class Order {
    private List<String> products = new ArrayList<>();

    public Order(List<String> products) {
        this.products = products;
    }

    public List<String> getProducts() {
        return products;
    }

}
